#include <iostream>
#include <string>
using namespace std;

struct Fibonacci {
    Fibonacci(unsigned max_) : max{max_} {}

    struct iterator {
        iterator(unsigned cnt) : count{cnt} {}
        void operator++() {
            auto f = f1 + f2;
            f1 = f2;
            f2 = f;
            ++count;
        }
        unsigned long operator*() const {
            return f2;
        }
        bool operator!=(const iterator& rhs) const {
            return count < rhs.count;
        }
    private:
        unsigned long f1    = 0;
        unsigned long f2    = 1;
        unsigned count = 0;
    };

    iterator begin() const { return {0}; }
    iterator end() const { return {max}; }
private:
    unsigned const max;
};

int main(int argc, char** argv) {
    auto n = argc==1 ? 10U : stoi(argv[1]);
    for (auto f : Fibonacci{n}) cout << f << "\n";
}
