#include <iostream>
#include <string>
#include <map>
using namespace std;

int main() {
    auto      freqs = map<string, unsigned>{};
    for (auto word  = ""s; cin >> word;) ++freqs[word];
    for (auto const& [word, count] : freqs)
        cout << word << ": " << count << "\n";
}
