#include <iostream>
#include <iomanip>
#include "length.hxx"
using namespace std;
using namespace std::literals;
using namespace ribomation::length;

int main() {
    auto distance = 2_km + 12.5_m - 0.5_mi + 31_ya;
    cout << "distance: " << fixed << setprecision(3) << distance << endl;
    return 0;
}
