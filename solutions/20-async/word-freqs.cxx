#include <iostream>
#include <iomanip>
#include <fstream>
#include <filesystem>

#include <string>
#include <string_view>
#include <memory>

#include <unordered_map>
#include <vector>
#include <algorithm>
#include <tuple>
#include <future>

#include <chrono>
#include <stdexcept>

using namespace std;
namespace fs = std::filesystem;
namespace ch = std::chrono;

using Freqs = unordered_map<string_view, unsigned>;
using Freq = pair<string_view, unsigned>;

auto load(const string& filename) -> tuple<unique_ptr<char>, unsigned long>;
auto launchTasks(string_view payload, unsigned num_tasks, unsigned min_size) -> vector<future<Freqs>>;
auto countTask(string_view payload, unsigned min_size) -> Freqs;
auto aggregateResults(vector<future<Freqs>>& taskResults) -> Freqs;


int main() {
    auto startTime = ch::high_resolution_clock::now();

    auto filename  = "../shakespeare.txt"s;
    auto max_words = 100U;
    auto min_size  = 5U;
    auto num_tasks = thread::hardware_concurrency();

    auto[ptr, size] = load(filename);
    auto payload   = string_view{ptr.get(), size};
    cout << "loaded " << payload.size() << " chars\n";

    auto taskResults = launchTasks(payload, num_tasks, min_size);
    auto results     = aggregateResults(taskResults);

    auto byFreqDesc = [](Freq lhs, Freq rhs) { return lhs.second > rhs.second; };
    auto sortable   = vector<Freq>{results.begin(), results.end()};
    partial_sort(sortable.begin(), sortable.begin() + max_words, sortable.end(), byFreqDesc);

    for_each_n(sortable.begin(), max_words, [](Freq f) {
        cout << setw(12) << left << f.first << ": " << f.second << "\n";
    });

    cout.imbue(locale{"en_US.UTF8"s});
    cout << "------\n";
    cout << "# Tasks     : " << num_tasks << "\n";
    cout << "File Size   : " << size << " bytes\n";
    cout << "Chunk Size  : " << (size / num_tasks) << " bytes\n";
    cout << "# Words     : " << sortable.size() << "\n";

    auto endTime = ch::high_resolution_clock::now();
    auto elapsed = ch::duration_cast<ch::milliseconds>(endTime - startTime);
    cout << "Elapsed Time: " << elapsed.count() << " ms\n";

    return 0;
}


auto load(const string& filename) -> tuple<unique_ptr<char>, unsigned long> {
    auto f = ifstream{filename};
    if (!f) throw invalid_argument{"cannot open "s + filename};

    auto size    = fs::file_size(filename);
    auto payload = unique_ptr<char>(new char[size]);
    f.read(payload.get(), static_cast<streamsize>(size));

    transform(payload.get(), payload.get() + size, payload.get(), [](char ch) {
        return ::tolower(ch);
    });

    return {std::move(payload), size};
}

auto launchTasks(string_view payload, unsigned num_tasks, unsigned min_size) -> vector<future<Freqs>> {
    auto chunkSize   = payload.size() / num_tasks;
    auto taskResults = vector<future<Freqs>>{};

    for (auto k = 0U; k < num_tasks; ++k) {
        auto chunk = payload.substr(k * chunkSize, chunkSize);
        auto task  = async(launch::async, [=]() { return countTask(chunk, min_size); });
        taskResults.push_back(std::move(task));
    }

    return taskResults;
}

auto countTask(string_view payload, unsigned min_size) -> Freqs {
    auto letters = "abcdefghijklmnopqrstuvwxyz"sv;
    auto freqs   = Freqs{};

    auto start = 0UL;
    do {
        start = payload.find_first_of(letters, start);
        if (start == string_view::npos) break;

        auto end = payload.find_first_not_of(letters, start);
        if (end == string_view::npos) break;

        auto word = payload.substr(start, end - start);
        if (word.size() >= min_size) ++freqs[word];

        start = end;
    } while (start < payload.size());

    return freqs;
}

auto aggregateResults(vector<future<Freqs>>& taskResults) -> Freqs {
    auto results = Freqs{};

    for (auto& result: taskResults) {
        for (auto&[word, freq]: result.get()) results[word] += freq;
    }

    return results;
}


