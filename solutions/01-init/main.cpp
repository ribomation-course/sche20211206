#include <iostream>
#include <string>
#include <set>

using namespace std;

int main() {
    set<string> words = {"hello"s, "from"s, "C++"s };
    for (auto w : words) cout << w << "\n";
}
