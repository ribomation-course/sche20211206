#include <iostream>
#include <initializer_list>
#include <numeric>
#include <algorithm>

using namespace std;

struct Stats {
    Stats(initializer_list<int> args) : cnt{static_cast<int>(args.size())} {
        sum = accumulate(args.begin(), args.end(), 0);
        min_ = *min_element(args.begin(), args.end());
        max_ = *max_element(args.begin(), args.end());
    }

    unsigned count() const { return cnt; }
    int avg() const { return sum / cnt; }
    int min() const { return min_; }
    int max() const { return max_; }

private:
    int cnt = 0;
    int sum = 0;
    int min_ = 0;
    int max_ = 0;
};

ostream& operator<<(ostream& os, const Stats& s) {
    return os << "Stats{cnt=" << s.count()
              << ", avg=" << s.avg()
              << ", min/max=" << s.min() << "/" << s.max()
              << "}";
}

int main() {
    Stats s = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    cout << s << "\n";
}
