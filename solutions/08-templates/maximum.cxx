#include <iostream>
#include <string>
using namespace std;

template<typename T>
T MAXIMUM(T a, T b) { return a >= b ? a : b; }

template<typename T>
T maximum(T a, T b, T c) { return MAXIMUM(MAXIMUM(a, b), c); }


int main() {
    cout << "int   : " << maximum(5, 10, 3) << endl;
    cout << "double: " << maximum(5.1, 10.3, 3.9) << endl;
    cout << "string: " << maximum("aa"s, "AA"s, "ZZ"s) << endl;
}
