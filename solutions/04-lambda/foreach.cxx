#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int numbers[] = {1,2,3,4,5,6,7,8,9,10};
    auto const N = sizeof(numbers) / sizeof(numbers[0]);

    auto f = [](auto n){ cout << n << " ";};
    for_each(numbers, numbers+N, f);
    cout<<"\n";

    auto factor = 42;
    transform(numbers, numbers+N, numbers, [factor](auto n){
        return n + factor;
    });

    for_each(&numbers[0], &numbers[N], [](auto n){ cout << n << " ";});
    cout<<"\n";

    for (auto it = &numbers[0]; it != &numbers[N]; ++it) {
        auto n = *it;
        cout << n << " ";
    }
    cout<<"\n";
}
