#include <iostream>
#include <filesystem>
#include <string>
#include <chrono>
#include "count.hxx"

using namespace std;
using namespace std::chrono;
using namespace ribomation::io;
namespace fs = std::filesystem;

int main(int argc, char** argv) {
    auto startTime = high_resolution_clock::now();

    auto dir = fs::path{argc == 1 ? "../.." : argv[1]};
    if (!fs::is_directory(dir)) { dir = fs::current_path(); }
    cout << "Dir: " << fs::canonical(dir) << endl;

    auto total = Count("TOTAL"s);
    for (const auto& e: fs::recursive_directory_iterator{dir}) {
        auto p = e.path();
        if (fs::is_regular_file(p) && Count::isTextFile(p)) {
            auto cnt = Count{p};
            total += cnt;
            cout << cnt << endl;
        }
    }
    cout << total << endl;

    auto endTime = high_resolution_clock::now();
    auto elapsed = duration_cast<milliseconds>(endTime - startTime);
    cout << "elapsed " << elapsed.count() << " ms\n";
}
