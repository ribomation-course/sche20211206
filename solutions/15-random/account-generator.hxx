#pragma once

#include <string>

namespace ribomation::util {
    using std::string;
    using namespace std::string_literals;

    extern float nextInterestRate(float lb = .25F, float ub = 5.F);
    extern int nextBalance(float mean = 1000.F, float stddev = 200.F);
    extern string nextAccno(string pattern = "SEB-###-#####"s);

}

