#include "account-generator.hxx"
#include <random>
#include <algorithm>
#include <iterator>

namespace {
    auto r = std::random_device{};
}

namespace ribomation::util {

    float nextInterestRate(float lb, float ub) {
        auto rates = std::uniform_real_distribution<float>{lb, ub};
        return rates(r); //rates.operator()(r)
    }

    int nextBalance(float mean, float stddev) {
        auto amounts = std::normal_distribution<float>{mean, stddev};
        return static_cast<int>(amounts(r));
    }

    string nextAccno(string pattern) {
        auto digits = std::uniform_int_distribution<char>{'0', '9'};

        string result;
        result.reserve(pattern.size());
        std::transform(pattern.begin(), pattern.end(), std::back_inserter(result), [&](auto ch) {
            return (ch == '#') ? digits(r) : ch;
        });

        return result;
    }

}
