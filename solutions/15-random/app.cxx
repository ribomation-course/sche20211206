#include <iostream>
#include <iomanip>
#include "account-generator.hxx"

using namespace std;
using namespace ribomation::util;

int main() {
    auto accno   = nextAccno("HB###-####-##"s);
    auto balance = nextBalance();
    auto rate    = nextInterestRate(.5F, 3.F);

    cout << "Account{"
         << accno << ", SEK "
         << balance << ", "
         << setprecision(1) << fixed << rate << "%}\n";
}
