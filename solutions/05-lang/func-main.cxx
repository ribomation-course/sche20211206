#include <iostream>

using namespace std;

namespace {
    unsigned sum(unsigned n) {
        return n * (n + 1) / 2;
    }
}
extern unsigned sum2(unsigned);

int main() {
    cout << "sum(1..10) = " << sum(10) << "\n";
    cout << "sum(1..10) = " << sum2(10) << "\n";
}
