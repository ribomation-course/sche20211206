
namespace {
    unsigned sum(unsigned n) {
        return n * (n + 1) / 2;
    }
}
// multiple definition of `sum(unsigned int)';
// func.cxx.o:/mnt/c/rib...ions/05-lang/func.cxx:3: first defined here

unsigned sum2(unsigned n) {
    return sum(n);
}
