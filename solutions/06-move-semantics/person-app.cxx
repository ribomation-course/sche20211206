#include <iostream>
#include <stdexcept>
#include <string>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

auto func(Person q) -> Person {
    cout << "[func] q: " << q << endl;
    q.incrAge();
    cout << "[func] q: " << q << endl;
    return q;
}

int main() {
    auto p = Person{"Cris P. Bacon", 27};
    cout << "[main] p: " << p << endl;

    cout << "-------\n";
    auto q = func(move(p));
    cout << "[main] p: " << p << endl;
    cout << "[main] q: " << q << endl;

    cout << "-------\n";
    p = move(q);
    cout << "[main] p: " << p << endl;
    cout << "[main] q: " << q << endl;
}
