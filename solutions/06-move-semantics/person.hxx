#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <cstring>

namespace ribomation {
    using std::string;
    using std::ostream;
    using std::ostringstream;
    using std::cout;
    using std::endl;

    class Person {
        char* name = nullptr;
        unsigned age = 0;

        static char* copystr(const char* str) {
            if (str == nullptr) return nullptr;
            return strcpy(new char[strlen(str) + 1], str);
        }

    public:
        Person(const char* n, unsigned a)
                : name{copystr(n)}, age{a} {
            cout << "Person(" << name << ", " << age << ") @ " << this << endl;
        }

        Person() {
            cout << "Person() @ " << this << endl;
            name = copystr("");
        }

        ~Person() {
            cout << "~Person() @ " << this << endl;
            delete[] name;
        }

        Person(const Person& that) = delete;
        auto operator=(const Person& that) -> Person& = delete;

        Person(Person&& that) noexcept: name{that.name}, age{that.age} {
            that.name = nullptr;
            that.age  = 0;
            cout << "Person(Person&& " << name << ") @ " << this << endl;
        }

        auto operator=(Person&& that) noexcept -> Person& {
            if (this != &that) {
                delete[] name;
                name = that.name;
                that.name = nullptr;
                age = that.age;
                that.age = 0;
            }
            cout << "operator=(Person&& " << name << ") @ " << this << endl;
            return *this;
        }

        unsigned incrAge() {
            return ++age;
        }

        [[nodiscard]] string toString() const {
            ostringstream buf{};
            buf << "Person(" << (name ? name : "<null>") << ", " << age << ") @ " << this;
            return buf.str();
        }

        friend auto operator<<(ostream& os, const Person& p) -> ostream& {
            return os << p.toString();
        }

    };

}
