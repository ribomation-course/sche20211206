#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <thread>
#include <chrono>
#include <vector>

using namespace std;
using namespace std::literals;
using namespace std::chrono;
using namespace std::chrono_literals;

struct Sync : ostringstream {
    ostream& os;
    explicit Sync(ostream& os) : os{os} {}
    ~Sync() override {
        (*this) << "\n";
        os << ostringstream::str();
        os.flush();
    }
};

string operator *(const string& s, unsigned n) {
    string r;
    while (n-- > 0) r += s;
    return r;
}

int main() {
    auto           numThreads  = 20U;
    auto           numMessages = 10'000U;
    vector<thread> threads;

    for (auto      k           = 0U; k < numThreads; ++k) {
        auto body = [numMessages](unsigned id) {
            for (auto i = 0U; i < numMessages; ++i) {
                Sync{cout} << ("    "s * id) << "[" << id << "]";
            }
        };
        threads.emplace_back(body, k + 1);
    }

    for (auto& t: threads) t.join();
    return 0;
}

