#include <iostream>
#include <tuple>
#include <map>

using namespace std;

auto fib(unsigned n) {
    if (n == 0) return 0U;
    if (n == 1) return 1U;
    return fib(n - 2) + fib(n - 1);
}

auto compute(unsigned n) {
    return make_tuple(n, fib(n));
}

auto list(unsigned n) {
    auto      result = map<unsigned, unsigned>{};
    for (auto k      = 1U; k <= n; ++k) {
        auto[arg, res] = compute(k);
        result[arg] = res;
    }
    return result;
}

int main() {
    auto n = 10U;
    {
        auto result = fib(n);
        cout << "fib(" << n << ") = " << result << "\n";
    }
    {
        auto[arg, result] = compute(n);
        cout << "fib(" << arg << ") = " << result << "\n";
    }
    {
        auto result = list(n);
        for (auto[arg, res]: result)
            cout << "fib(" << arg << ") = " << res << "\n";
    }
}
