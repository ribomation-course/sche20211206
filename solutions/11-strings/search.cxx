#include <iostream>
#include <string>
#include <stdexcept>
#include <tuple>

using namespace std;
using namespace std::literals;

// Usage:
// ./search Aramis < musketeers.txt

int main(int numArgs, char* args[]) {
    if (numArgs <= 1) {
        throw invalid_argument("usage: "s + args[0] + " {phrase} < {input}"s);
    }
    string phrase = args[1];

    for (auto[line, lineno] = make_tuple(""s, 1U); getline(cin, line); ++lineno) {
        if (line.find(phrase) != string::npos) {
            cout << lineno << ": " << line << endl;
        }
    }
    return 0;
}
