#include <iostream>
#include <chrono>

using namespace std;
using namespace std::chrono;

int main() {
    auto duration = 3h + 7min + 49s;
    cout << "duration: " << duration.count() << " seconds\n";
}
