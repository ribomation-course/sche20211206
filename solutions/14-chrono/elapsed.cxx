#include <iostream>
#include <fstream>
#include <iomanip>
#include <tuple>
#include <chrono>
#include <functional>
#include <unordered_map>
#include <vector>
#include <stdexcept>
#include <filesystem>
#include <memory>
#include <string_view>

using namespace std;
namespace ch = std::chrono;
namespace fs = std::filesystem;

using FreqPair = pair<string, unsigned>;
using WordFreqs = unordered_map<string, unsigned>;

template<typename RetType>
auto elapsed(function<RetType()> func) -> tuple<RetType, long> {
    auto start  = ch::high_resolution_clock::now();
    auto result = func();
    auto end    = ch::high_resolution_clock::now();
    auto time   = ch::duration_cast<ch::milliseconds>(end - start).count();
    return make_tuple(result, time);
}

auto aggregateFrequencies(const string& filename, unsigned max_words, unsigned min_size) -> vector<FreqPair>;

int main() {
    auto filename  = "../musketeers.txt"s;
    auto max_words = 100U;
    auto min_size  = 5U;

    auto[result, time] = elapsed<vector<FreqPair>>([=]() {
        return aggregateFrequencies(filename, max_words, min_size);
    });

    for_each(result.begin(), result.end(), [](auto const& f) {
        cout << setw(12) << left << f.first << ": " << f.second << "\n";
    });
    cout << "elapsed " << time << " ms\n";
}

auto aggregateFrequencies(const string& filename, unsigned int max_words, unsigned int min_size) -> vector<FreqPair> {
    auto f = ifstream{filename};
    if (!f) throw invalid_argument{"cannot open "s + filename};

    auto size = fs::file_size(filename);
    auto ptr  = unique_ptr<char>(new char[size]);
    f.read(ptr.get(), static_cast<streamsize>(size));

    transform(ptr.get(), ptr.get() + size, ptr.get(), [](char ch) {
        return ::tolower(ch);
    });

    auto payload = string_view{ptr.get(), size};
    auto letters = "abcdefghijklmnopqrstuvwxyz"sv;
    auto freqs   = WordFreqs{};

    auto start = 0UL;
    do {
        start = payload.find_first_of(letters, start);
        if (start == string_view::npos) break;

        auto end = payload.find_first_not_of(letters, start);
        if (end == string_view::npos) break;

        auto word = payload.substr(start, end - start);
        if (word.size() >= min_size) ++freqs[string{word.data(), word.size()}];

        start = end;
    } while (start < payload.size());

    auto byFreqDesc = [](FreqPair lhs, FreqPair rhs) { return lhs.second > rhs.second; };
    auto sortable   = vector<FreqPair>{freqs.begin(), freqs.end()};
    partial_sort(sortable.begin(), sortable.begin() + max_words, sortable.end(), byFreqDesc);

    auto result = vector<FreqPair>{};
    result.reserve(max_words);
    copy_n(sortable.begin(), max_words, back_inserter(result));

    return result;
}
