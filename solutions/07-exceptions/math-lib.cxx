#include <sstream>
#include <iostream>
#include <string>
#include <regex>
#include <cmath>
#include <cstring>
#include <cerrno>
#include "math-lib.hxx"

namespace ribomation {
    using namespace std::string_literals;
    using std::ostringstream;
    using std::string;

    auto log_2(double x) -> double {
        string const   filename = __FILE__;
        unsigned const lineno   = __LINE__;

        auto           result   = std::log2(x);
        if (std::isfinite(result)) {
            return result;
        }

        if (std::isnan(result)) {
            throw MathError{filename, lineno, "log"s, x, "NaN"s};
        }

        if (std::isinf(result)) {
            throw MathError{filename, lineno, "log"s, x, "oo"s};
        }

        throw MathError{filename, lineno, "log"s, x, strerror(errno)};
    }

    string MathError::fmt(const string& file, unsigned line, const string& func, double arg, const string& what) {
        auto buf = ostringstream{};
        buf << file.substr(file.find_last_of('/') + 1) << ":" << line << " - " << func << "(" << arg << ") --> " << what;
        return buf.str();
    }

}
