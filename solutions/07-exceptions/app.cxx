#include <iostream>
#include "math-lib.hxx"
using namespace std;
using namespace ribomation;

void invoke(double x) {
    try {
        cout << "log[2]("<<x<<") = " << log_2(x) << endl;
    } catch (const MathError& x) {
        cout << "ERROR: " << x.what() << endl;
    }
}

int main() {
    invoke(1024);
    invoke(0);
    invoke(-17);
}
