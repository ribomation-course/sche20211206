#pragma once

#include <stdexcept>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

namespace ribomation {
    using std::runtime_error;
    using std::string;
    using std::ostringstream;
    using std::vector;

    struct MathError : runtime_error {
        explicit MathError(const string& file, unsigned line,
                           const string& func, double arg,
                           const string& what)
                : runtime_error{fmt(file, line, func, arg, what)} {}

        static string fmt(const string& file, unsigned line,
                          const string& func, double arg,
                          const string& what);
    };

    auto log_2(double x) -> double;
}
